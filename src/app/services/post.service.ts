import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Post } from '../models/Post';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {
  // what is the url in the BACKEND for our GetMapping method?
  // http://localhost:8080/api/v1/posts

  private getUrl: string = "http://localhost:8080/api/v1/posts"

  // using dependency injection bring in the httpClient
  constructor(private httpClient: HttpClient) { }

  // create a method that will make a request to the GetMapping method in our backend
  getPosts(): Observable<Post[]> {
    return this.httpClient.get<Post[]>(this.getUrl).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the post mapping method 
  // in our backend
  savePost(newPost: Post): Observable<Post> {
    return this.httpClient.post<Post>(this.getUrl, newPost);
  }

  // method that will make a request to the GetMapping method
  // to view an individual post from our backend
  viewPost(id: number): Observable<Post> {
    return this.httpClient.get<Post>(`${this.getUrl}/${id}`).pipe(
      map(result => result)
    )
  }

  // method that will make a request to the delete mapping method
  // to delete an individual post from our backend
  deletePost(id: number): Observable<any> {
    return this.httpClient.delete(`${this.getUrl}/${id}`, {responseType: 'text'})
  }

}
