import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddPostComponent } from './components/add-post/add-post.component';
import { ListPostsComponent } from './components/list-posts/list-posts.component';

const routes: Routes = [
  {path: 'posts', component: ListPostsComponent},
  {path: 'add-post', component: AddPostComponent},
  {path: 'edit-post/:id', component: AddPostComponent},
  {path: '', redirectTo: '/posts',pathMatch: 'full'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
